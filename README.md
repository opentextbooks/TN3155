<!-- #region -->
# Quantum Sensing and Measurement

Lecture notes and teaching material used for the Delft University of Technology course on Quantum Sensing and Measurement.

The compiled materials are available at https://interactivetextbooks.tudelft.nl/tn3155


## Setting up Local build env

```
conda create --name=qsm-jb python=3.7 
conda activate qsm-jb
conda install pip 
pip install -r requirements.txt
```

## Local build process

```
jb build .
```

This will create a `_build` folder and from there you can directly open the local HTML files.

I do this on my computer by:

```
open  file:///Users/gsteele/Documents/GitHub/TN3155/_build/html/index.html`
```

## Gary notes 28 feb 2024

Hide code cell: 

* In Jup notebook interface, go to View => Cell Toolbar => Tags, then add tag "hide-input"

Tried this, but didn't work. Note that I am using notebook server and I am also using Jupytext, and in the past, I had tagged these for "pair with markdown", and I think this is a problem:

```
WARNING: multiple files found for the document "src/3_noise_in_HOs": ['src/3_noise_in_HOs.md', 'src/3_noise_in_HOs.ipynb']
```

It raises the question though: should my JB files be notebooks or markdown notebooks? 
<!-- #endregion -->

```python

```
